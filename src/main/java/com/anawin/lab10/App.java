package com.anawin.lab10;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       Rectangle rec = new Rectangle(5,3);
       System.out.println(rec.toString());
       System.out.printf("%s area: %.1f \n",rec.getName(),rec.calArea());
       System.out.printf("%s perimeter: %.1f \n",rec.getName(),rec.calPerimeter());

       Rectangle rec2 = new Rectangle(2,2);
       System.out.println(rec2.toString());
       System.out.printf("%s area: %.1f \n",rec2.getName(),rec2.calArea());
       System.out.printf("%s perimeter:: %.1f \n",rec2.getName(),rec2.calPerimeter());

       Circle circle1 = new Circle(2);
       System.out.println(circle1.toString());
       System.out.printf("%s area: %.2f \n",circle1.getName(),circle1.calArea());
       System.out.printf("%s perimeter: %.2f \n",circle1.getName(),circle1.calPerimeter());

       Circle circle2 = new Circle(3);
       System.out.println(circle2.toString());
       System.out.printf("%s area: %.2f \n",circle2.getName(),circle2.calArea());
       System.out.printf("%s perimeter: %.2f \n",circle2.getName(),circle2.calPerimeter());

       Triangle triangle1 = new Triangle(4, 5, 6);
       System.out.println(triangle1.toString());
       System.out.printf("%s area: %.2f \n",triangle1.getName(),triangle1.calArea());
       System.out.printf("%s perimeter: %.2f \n",triangle1.getName(),triangle1.calPerimeter());

       Triangle triangle2 = new Triangle(10, 11, 12);
       System.out.println(triangle2.toString());
       System.out.printf("%s area: %.2f \n",triangle2.getName(),triangle1.calArea());
       System.out.printf("%s perimeter: %.2f \n",triangle2.getName(),triangle1.calPerimeter());

    }
}