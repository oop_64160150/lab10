package com.anawin.lab10;

public class Triangle extends Shape {
    private double a;
    private double b;
    private double c;

    public Triangle(double a, double b, double c) {
        super("Triangle");
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getA() {
        return a;
    }
    public double getB() {
        return b;
    }
    public double getC() {
        return c;
    }
    @Override
    public String toString(){
        return this.getName()+" a:"+this.a+" b:"+this.b+" c:"+this.c;
    }

    @Override
    public double calArea() {
        double area = 0;
        double s = 0;
        s = (a+b+c)/2;
        area = Math.sqrt(s*(s-a)*(s-b)*(s-c));
        return area;
    }

    @Override
    public double calPerimeter() {
        double pari = 0 ;
        pari = a+b+c;
        return pari;
    }
}
